<?php

namespace Azak1r\Buyback\Parsers;

use Seat\Eveapi\Models\Sde\InvType;
use DB;
use Seat\Eveapi\Models\Sde\InvGroup;

class marketParser
{
    public function getESIPricesForTypes($typeIds)
    {
        $results = array();

		if (count($typeIds) == 1 && is_array($typeIds[0]))
		{
			$typeIds = $typeIds[0];
		}

		if (count($typeIds) > 0)
		{
            $chunks = array_chunk($typeIds, 20);

            foreach($chunks as $chunk)
            {
                foreach($chunk as $i => $materialID)
                {
                    $price = DB::table('market_prices')->where('type_id', $materialID)->get()->toArray();
                    $results[$materialID] = $price;
                }
            }
        }
        return $results;
    }

    public function getBuybackPricesForTypes($typeIds)
    {
        $results = array();

        // Get only Unique TypeIds
        $uniqueTypeIds = array_values(array_unique($typeIds));

        // TODO:: Setup cache system to speed up page loads

        // Get updated Stats from ESI Database
        $priceResults = $this->getESIPricesForTypes($uniqueTypeIds);

        // Verify typeid's are ore/ice for refining If they are remove the typeID from the Unique list and replace it with the material name
        foreach ($priceResults as $key => $item)
        {
            $groupId = InvType::find($item[0]->type_id)->group()->pluck('groupID');
            $category = InvGroup::find($groupId);

                if($category[0]->categoryID == 25)
                {
                    // Temporarily remove ore/ice from array for material processing
                    unset($priceResults[$key]);

                    $materials = $this->getRefinedMaterialsForType($item);
                    $materialPrices = $this->getESIPricesForTypes(array_keys($materials));

                    foreach ($materials as $materialID => $quantity)
                    {
                        $price = 0.0;
                        $itemPrice = $materialPrices[$materialID][0]->adjusted_price * $quantity;

                        $price = $price + $itemPrice;
                    }

                    // Divide total price by 100 (100 units needed for refining)
                    $results[$key] = $price / 100;
                } else {
                    $results[$key] = $item[0]->adjusted_price;
                }
        }
        return $results;
    }

    public function getRefinedMaterialsForType($typeId)
    {
        $results = array();
        $refinerate = 86;

        $item = InvType::find($typeId[0]->type_id);
        $materials = $item->materials()->pluck('quantity', 'materialTypeID');

        foreach($materials as $materialID => $quantity)
        {
            $results[$materialID] = $quantity * ($refinerate / 100);
        }
        return $results;
    }

    public function generateID($limit)
    {
        $code = '';
        for($i = 0; $i < $limit; $i++)
        {
            $code .=mt_rand(0,9);

        }
        return $code;
    }
}
