<?php

namespace Azak1r\Buyback\Parsers;

class LineItemEntity
{
    public function __construct()
    {
        $this->isValid = true;
    }

    protected $id;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    protected $typeId;

    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    public function getTypeId()
    {
        return $this->typeId;
    }

    protected $name;

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    protected $isValid;

    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getIsValid()
    {
        return $this->isValid;
    }
}
