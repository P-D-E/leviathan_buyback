<?php

namespace Azak1r\Buyback\Http\Controllers;

use Azak1r\Buyback\Models\BuyBackQuotes;
use Azak1r\Buyback\Models\MarketOrders;
use Illuminate\Http\Request;
use Seat\Web\Http\Controllers\Controller;
use Azak1r\Buyback\Parsers\buyBackParser;
use Azak1r\Buyback\Parsers\marketParser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class FrontendController extends Controller
{
    public function index()
    {
        return view('buyback::frontend.index');
    }

    public function calculator()
    {
        return view ('buyback::frontend.calculator');
    }

    public function calculate(Request $request)
    {
        $parser = new buyBackParser();
        $rawData = $request->get('pasteArea');
        $items = $parser->parseInputData($rawData);
        $hasInvalid = false;

        // Check to make sure it parsed correctly
		if ($rawData == null || count($items) <= 0)
		{
            return Redirect::back()->withErrors(['No Valid Items detected!']);
        }

        $typeIds = array();

        // Grab our TypeID's to pull pricing for
		foreach ($items as $item)
		{
			$typeIds[] = $item['typeid'];
        }
        $market = new marketParser();
        $typePrices = $market->getBuybackPricesForTypes($typeIds);
        
        $offer = 0;

        // Populate our Line Items from Pricing information
        for ($i = 0; $i < count($items); $i++)
		{
            // Check if price is -1, means Can Buy is False
            if ($typePrices[$items[$i]['typeid']] == -1 | $items[$i]['typeid'] == 0)
            {
                
                $items[$i]['unitPrice'] = 0;
				$items[$i]['netPrice'] = 0;
				$items[$i]['grossPrice'] = 0;
				$items[$i]['isValid'] = false;

				$hasInvalid = true;
            } else
            {
                $items[$i]['unitPrice'] = $typePrices[$items[$i]['typeid']] * 0.9;
                $items[$i]['netPrice'] = $items[$i]['quantity'] * $typePrices[$items[$i]['typeid']] * 0.9;
                //dd($items[$i]['netPrice']);
            }

            $offer += $items[$i]['netPrice'];
        }

        $data = array();
        $data['items'] = $items;
        $data['details'] = $typePrices;

        $cId = $market->generateID(10);

        // Store to Database
        $quote = new BuyBackQuotes();
        $quote->quote_id = $cId;
        $quote->offer = $offer;
        $quote->data = serialize($data);
        $quote->created_at = Carbon::now();
        $quote->save();

        $request->flashExcept('_token');

        //return view ('tools.buyback.calculator', compact(['data', 'offer', 'cId', 'rawData']))->with('cid', $cId);
        return redirect()->route('buyback.show', [$cId]);

    }

    public function show($cId)
    {
        $quote = BuyBackQuotes::find($cId);

        $offer = $quote->offer;
        $data = unserialize($quote->data);

        return view ('buyback::frontend.calculator', compact(['data', 'offer', 'cId']));
    }
}