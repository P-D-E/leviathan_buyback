<?php

/*
|--------------------------------------------------------------------------
| Buyback Frontend Routes
|--------------------------------------------------------------------------
|
| This is where all the routes can be found that are added by this package that function on the frontend of things.
|
*/

Route::group([
    'namespace' => 'Azak1r\Buyback\Http\Controllers',
], function() {
    Route::group([
        'middleware'    => ['web'],
    ], function() {
        Route::get('/buyback', [
            'as'            => 'buyback.index',
            'uses'          => 'FrontendController@index'
        ]);
        Route::get('/calculator', [
            'as'            => 'buyback.calculator',
            'uses'          => 'FrontendController@calculator'
        ]);
        Route::post('/calculate', [
            'as'            => 'buyback.calculate',
            'uses'          => 'FrontendController@calculate'
        ]);
        Route::get('/show/{cId}', [
            'as'            => 'buyback.show',
            'uses'          => 'FrontendController@show'
        ]);
    });
});
/**Route::group(['prefix' => 'buyback'], function () {
    Route::get('/', 'BuyBackController@index')->name('buyback.index');
    Route::get('/calculator', 'BuyBackController@calculator')->name('buyback.calculator');
    Route::post('/calculate', 'BuyBackController@calculate')->name('buyback.calcate');
    Route::get('/show/{cId}', 'BuyBackController@show')->name('buyback.show');
});**/