<?php

namespace Azak1r\Buyback\Models;

use Illuminate\Database\Eloquent\Model;

class BuyBackQuotes extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected static $unguarded = true;

    protected $primaryKey = 'quote_id';

    protected $table = 'buyback_quotes';
}
