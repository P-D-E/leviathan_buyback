<?php

namespace Azak1r\Buyback;

use Illuminate\Support\ServiceProvider;

/**
 * Class BuybackServiceProvider
 * @package Azak1r/Leviathan
 */
class BuybackServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->add_routes();

        $this->add_views();

        $this->add_migrations();
    }

    /**
     * Include the routes.
     */
    public function add_routes()
    {

        if (! $this->app->routesAreCached()) {
            include __DIR__ . '/Http/routes.php';
        }
    }

     /**
     * Set the path and namespace for the views.
     */
    public function add_views()
    {

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'buyback');
    }

    /**
     * Include the migrations
     */
    public function add_migrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
    }    
}
