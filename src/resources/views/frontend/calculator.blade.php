@extends('layouts.master')

@section('title', 'Buyback Calculator')

@section('content')
<div class="nk-gap-4"></div>
<div class="container">
    <div class="nk-info-box bg-main-1">
        <div class="nk-info-box-icon">
            <i class="ion-information-circled"></i>
        </div>
        The buyback tool is still in Alpha phase. Please keep this in mind when using it. Report any issues to <a href="https://evewho.com/character/1638701812" target="_blank" style="color: white">Azakir Nalda</a> ingame or, azakir#9174 on discord!
    </div>

    <div class="nk-info-box bg-danger">
        <div class="nk-info-box-icon">
            <i class="ion-information-circled"></i>
        </div>
        Right now the tool does not work for named items such as containers, blueprints and ships. If you want to get a price for a named ship just fill in the ship hull name.
    </div>

    @if($errors->any())
    <div class="nk-info-box bg-danger">
        <div class="nk-info-box-icon">
            <i class="ion-alert-circled"></i>
        </div>
        {{$errors->first()}}
    </div>
    @endif
    <div class="row vertical-gap">
        <div class="col-md-6">
            <div class="nk-box-3">
                <h2 class="nk-title h3 text-center">Presumed Dead Buy Back</h2>
                <div class="nk-gap-1"></div>
                <form method="post" action="{{ route('buyback.calculate') }}">
                    @csrf
                    <div class="form-group">
                        <label for="pasteArea">Paste assets below</label>
                    <textarea class="form-control" name= "pasteArea" id="pasteArea" rows="10" >{{ old('pasteArea') }}</textarea>
                    </div>
                    <button class="nk-btn nk-btn-lg link-effect-4">Get quote</button>
                </form>
            </div>
        </div>
        @isset($cId)
        <div class="col-md-6">
            <div class="nk-box-3">
            <h2 class="nk-title h2 text-center">Buyback ID: {{$cId}}
            <input style="position: absolute; left: -1000px; top: -1000px;" type="text" name="quoteID" value="{{$cId}}" id="quoteIDBox" readonly>
                <a href="#"><i class="ion-ios-copy-outline" onclick="copyToClipboard('quoteIDBox');" title="Copy To Clipboard"></i></a></h2>
            <div class="box-body">
                <p class="offer" style="font-weight: 700;font-size: 22px;text-align:center;">
                    Buyback Value: {{ number_format($offer,2,'.',',') }} <small>ISK</small>
                    <input style="position: absolute; left: -1000px; top: -1000px;" type="text" name="quoteValue" value=" {{ number_format($offer,2) }}" id="quoteValueBox" readonly>
                    <a href="#"><i class="ion-ios-copy-outline" onclick="copyToClipboard('quoteValueBox');" title="Copy To Clipboard"></i></a></h2>
            </p>
            </div>
            <div class="nk-gap-1"></div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width:20%;">Quantity</th>
                        <th></th>
                        <th>Item</th>
                        <th style="width:20%;text-align:right;">Unit Price</th>
                        <th style="width:20%;text-align:right;">Net Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['items'] as $item)
                    <tr>
                        <td>{{ number_format($item['quantity'],0,'.',',') }}</td>
                        <td><img src="https://image.eveonline.com/Type/{{ $item['typeid'] }}_64.png" style="max-width:32px;"/></td>
                        <td> {{ $item['name'] }}</td>
                        <td style="width:20%;text-align:right;">{{ number_format($item['unitPrice'],2,'.',',') }}</td>
                        <td style="width:20%;text-align:right;">{{ number_format($item['netPrice'],2,'.',',') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        @endisset
    </div>
</div>
<div class="nk-gap-4"></div>
<div class="nk-gap-4"></div>
@endsection

@section('script')
<script>
    function copyToClipboard(ibox) {
	document.getElementById(ibox).select();
	document.execCommand('Copy');
    }
</script>
@endsection
