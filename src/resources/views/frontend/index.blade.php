@extends('layouts.master')

@section('title', 'Buyback')

@section('content')
<div class="nk-header-title nk-header-title-lg nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image">
        <img src="/img/Mining.png" alt="" class="jarallax-img">
    </div>
    <div class="nk-header-table">
        <div class="nk-header-table-cell">
            <div class="container">
                <div class="nk-header-text">
                    <img src="/img/logo.png" alt="" width="128">
                    <h1 class="nk-title display-3">Presumed Dead Enterprises Buyback System</h1>
                    <div> Save time and money by using the Buyback systems instead of market orders. <br> Get more fun out of your game time by using the automated calculators. A safe and fair way to sell your goodies.</div>
                    <div class="nk-gap-2"></div>
                <a href="{{ route('buyback.calculator') }}" class="nk-btn nk-btn-lg nk-btn-color-main-1 link-effect-4">
                        <span>Calculate your price (WIP)</span>
                    </a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#more" class="nk-btn nk-btn-lg link-effect-4">
                        <span>Learn More</span>
                    </a>
                    <div class="nk-gap-4"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="more" class="nk-box text-center text-white bg-dark-1">
    <div class="bg-image bg-image-parallax op-5">
        <img src="//vahrokh.com/wp-content/uploads/2012/05/2012-04-18_TRTNM_W_01.png" alt="" class="jarallax-img">
    </div>
    <div class="nk-gap-5"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h2 class="nk-title">Presumed Dead Enterprises Buyback System</h2>
                <h3 class="nk-sub-title text-main-1">Eve Online Buyback System</h3>
                <div class="nk-gap"></div>
                <p>Wouldn’t it be nice if you could sell all of your items at once without the need to compare the buy and sell prices for every single item via the market browser? Well now you can!</p>
                <p>Presumed Dead Enterprises allows you to sell any of your items to our corporation by using a single contract! It saves you precious game time from hauling to market hubs and comparing buy orders, we also pay you a price that is fair for everyone.</p>
                <p>There are two main reasons why we are able to offer you decent prices. The first reason is that the system is not designed to rip you off, it is designed to help our corp and alliance members out. The second reason for the prices is that when we are in demand of some specific items the the buyback system is one channel to acquire them. By reprocessing, relocating and using the most efficient methods of refining the raw products in to high end products we are able to cut some profit even with these prices.</p>
            </div>
        </div>
    </div>
    <div class="nk-gap-5"></div>
    <div class="nk-gap-4"></div>
</div>

<div class="container">
    <div class="nk-gap-6"></div>
    <div class="nk-gap-2"></div>
    <div class="row vertical-gap lg-gap">
        <div class="col-md-4">
            <div class="nk-ibox">
                <div class="nk-ibox-cont">
                    <h2 class="nk-ibox-title">All items accepted</h2> The tool will calculate the exact price for each item type. Depending on the demand for the items a tax is applied on them. The higher the demand for an item the lower the tax will be!
                </div>
            </div>
            <div class="nk-gap-2"></div>
            <div class="text-center">
                <img src="/img/icons/24_64_14.png">
                <img src="/img/icons/23_64_5.png">
                <img src="/img/icons/51_64_3.png">
            </div>
        </div>
        <div class="col-md-4">
            <div class="nk-ibox">
                <div class="nk-ibox-cont">
                    <h2 class="nk-ibox-title">Values based on refining</h2> You don't have to refine your own items to get the best value for them or have to worry about those refining skills. Most item prices are based on their actual refined content. Even when you sell the raw unprocessed items!
                </div>
            </div>
            <div class="nk-gap-2"></div>
            <div class="text-center">
                <img src="/img/icons/refinery_T2Reactions.png">
                <img src="/img/icons/17_128_1.png" style="width: 64px;">
                <img src="/img/icons/50_64_6.png">
            </div>
        </div>
        <div class="col-md-4">
            <div class="nk-ibox">
                <div class="nk-ibox-cont">
                    <h2 class="nk-ibox-title">Fair and easy</h2> We have designed the system to help you get more fun out of your gametime. You don't have to go and wrestle with market pvp in trade hubs or all the hauling work. All prices and taxes are visible to you.
                </div>
            </div>
            <div class="nk-gap-2"></div>
            <div class="text-center">
                <img src="/img/icons/17_128_2.png" style="width: 64px;">
                <img src="/img/icons/6_64_3.png">
                <img src="/img/icons/32_128_3.png" style="width: 64px;">
            </div>
        </div>
    </div>
    <div class="nk-gap-2"></div>
    <div class="nk-gap-6"></div>
</div>

<div class="container">
    <div class="nk-gap-2"></div>
    <div class="row align-items-center">
        <div class="col-md-6">
            <div class="nk-box-3 bg-dark-1">
                <div class="nk-ibox">
                    <img src="//cdnb.artstation.com/p/assets/images/images/003/183/127/large/daniel-brown-23.jpg?1470746642" style="width: 100%;">
                        <div class="nk-gap-2"></div>
                    <div class="nk-ibox-cont">
                        <h2 class="nk-ibox-title">EASY TO READ AND USE</h2>
                        <h3><small>Probably the only easy thing in EVE</small></h3>
                        <div class="nk-divider"></div>
                        <p>Using the buyback calculator is probably the easiest thing you can do in EVE. It is simply a matter of copy passing your items and creating a contract! The calculator will give you easy to read information about your items and notify you clearly in case some of the products are missing prices</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="nk-box-3 bg-dark-1">
                <div class="nk-ibox">
                    <img src="//live.staticflickr.com/478/30867017884_e38d5e47ce_b.jpg" style="width: 100%;">
                        <div class="nk-gap-2"></div>
                    <div class="nk-ibox-cont">
                        <h2 class="nk-ibox-title">ABOUT THE BACKEND CODE</h2>
                        <h3><small>It's like black magic!</small></h3>
                        <div class="nk-divider"></div>
                        <p>The buyback system is written with laravel just like the rest of the Leviathan platform. It pulls its price data from various sources (ESI and Evepraisal). Price data is update on a set frequency (roughly every 20 minutes) to ensure the prices you get are as close to the real prices as possible. The buyback part of Leviathan is closed source and maintained by Azakir Nalda. For more technical details please contact me ingame.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nk-gap-2"></div>
    <div class="nk-gap-6"></div>
</div>

<div class="container text-center">
    <div class="nk-gap-6"></div>
    <div class="nk-gap-2"></div>
    <div class="row vertical-gap lg-gap justify-content-center">
        <h3>OPERATING THE CALCULATOR</h3>
        <p>To launch the calculator please click on the calculate prices button on the buyback page. After this you will have to select your location that you want to create your contract at. Additionally you can select to donate a percentage amount of your final contract value to the corporation. For step by step guide how to operate and read the calculator please see the how to sell page</p>
    </div>
    <div class="nk-gap-2"></div>
    <div class="nk-gap-6"></div>
</div>
@endsection
